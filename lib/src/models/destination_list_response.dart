// To parse this JSON data, do
//
//     final destinationListResponse = destinationListResponseFromJson(jsonString);

import 'dart:convert';

List<DestinationListResponse> destinationListResponseFromJson(String str) =>
    List<DestinationListResponse>.from(
        json.decode(str).map((x) => DestinationListResponse.fromJson(x)));

String destinationListResponseToJson(List<DestinationListResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DestinationListResponse {
  DestinationListResponse({
    this.cc,
    this.n,
    this.k,
    this.hc,
    this.h,
    this.p,
    this.t,
    this.tn,
  });

  final String cc;
  final String n;
  final String k;
  final String hc;
  final String h;
  final List<String> p;
  final String t;
  final String tn;

  factory DestinationListResponse.fromJson(Map<String, dynamic> json) =>
      DestinationListResponse(
        cc: json["cc"] == null ? null : json["cc"],
        n: json["n"] == null ? null : json["n"],
        k: json["k"] == null ? null : json["k"],
        hc: json["hc"] == null ? null : json["hc"],
        h: json["h"] == null ? null : json["h"],
        p: json["p"] == null
            ? null
            : List<String>.from(json["p"].map((x) => x)),
        t: json["t"] == null ? null : json["t"],
        tn: json["tn"] == null ? null : json["tn"],
      );

  Map<String, String> toJson() => {
        "cc": cc == null ? null : cc,
        "n": n == null ? null : n,
        "k": k == null ? null : k,
        "hc": hc == null ? null : hc,
        "h": h == null ? null : h,
        "t": t == null ? null : t,
        "tn": tn == null ? null : tn,
      };
}
