import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/constants.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class RoomPersonCount extends StatefulWidget {
  final String counterLabel;
  final int counterValue;
  final Function incFunction;
  final Function decFunction;

  RoomPersonCount(
      {this.counterLabel,
      this.decFunction,
      this.incFunction,
      this.counterValue});

  @override
  _RoomPersonCountState createState() => _RoomPersonCountState();
}

class _RoomPersonCountState extends State<RoomPersonCount> {
  // int c = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ListTile(
          title: Row(
            children: <Widget>[
              Container(
                width: screenWidth(context, dividedBy: 4),
                child: new Text(
                  widget.counterLabel,
                  style: TextStyle(
                    fontSize: 20,
                    color: Constants.kitGradients[2],
                    fontWeight: FontWeight.w300,
                    fontStyle: FontStyle.normal,
                    fontFamily: 'Poppins'
                  ),
                ),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 2.5),
              ),
              GestureDetector(
                child: SvgPicture.asset("assets/icons/DecrementIcon.svg",
                         height:screenHeight(context,dividedBy: 25), width: screenWidth(context,dividedBy: 25),),
                onTap: () {
                  widget.decFunction();
                  // setState(() {
                  //   c = widget.counterValue;
                  // });
                },
              ),
              SizedBox(
                width: 10,
              ),
              Text(widget.counterValue.toString(),
                  style: TextStyle(
                      color: Constants.kitGradients[2], fontSize: 18,fontWeight: FontWeight.w400,fontStyle: FontStyle.normal,fontFamily: 'Poppins')),
              SizedBox(
                width: 10,
              ),
              GestureDetector(
                  child: SvgPicture.asset("assets/icons/IncrementIcon.svg",
                    height:screenHeight(context,dividedBy: 25), width: screenWidth(context,dividedBy: 25),
                      ),
                  onTap: () {
                    widget.incFunction();
                    // setState(() {
                    //   c = widget.counterValue;
                    // });
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
