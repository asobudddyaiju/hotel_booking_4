import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hote_booking_4/src/ui/screens/Room_selection_page.dart';
import 'package:hote_booking_4/src/ui/widgets/room_person_totalcount.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/guest_bottom_sheet.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/room_person_count.dart';
import 'package:hote_booking_4/src/utils/object_factory.dart';
import 'package:hote_booking_4/src/utils/utils.dart';

class RoomSelection extends StatefulWidget {
  @override
  _RoomSelectionState createState() => _RoomSelectionState();
}
String dates;
String guest;
String checkInDate;
String checkOutDate;
String nights;
String rooms;
String adults;
String children;
String monthInLetterCheckIn;
String monthInLetterCheckOut;


class _RoomSelectionState extends State<RoomSelection> {
  @override
  void initState() {
    // setState(() {
    //   monthInLetterCheckOut = ObjectFactory()
    //       .getMonthAlpha
    //       .getCheckInMonth(Hive.box('code').get(6));
    // });
    //
    // if (Hive.box('adult').get(5) != null) {
    //   setState(() {
    //     nights = Hive.box('adult').get(5).toString();
    //   });
    // } else {
    //   nights = "No: of";
    // }
    // if (Hive.box('code').get(3) != null) {
    //   setState(() {
    //     checkInDate =
    //         Hive.box('code').get(3).toString() + " " + monthInLetterCheckIn;
    //   });
    // } else {
    //   setState(() {
    //     checkInDate = "Checkin";
    //   });
    // }
  //   if (Hive.box('code').get(6) != null) {
  //     setState(() {
  //       checkOutDate = "- " +
  //           Hive.box('code').get(4).toString() +
  //           " " +
  //           monthInLetterCheckOut;
  //     });
  //   } else {
  //     setState(() {
  //       checkOutDate = "Checkout";
  //     });
  //   }
  //
    if (Hive.box('adult').get(1) != null) {
      setState(() {
        rooms = Hive.box('adult').get(1) + " Room";
      });
    } else {
      setState(() {
        rooms = "";
      });
    }
    if (Hive.box('adult').get(2) != null) {
      setState(() {
        adults = "- "+Hive.box('adult').get(2) + " Adult - ";
      });
    } else {
      setState(() {
        adults = "";
      });
    }
    if (Hive.box('adult').get(3) != null) {
      setState(() {
        children = Hive.box('adult').get(3) + " Children";
      });
    } else {
      setState(() {
        children = "";
      });
    }
    if ((Hive.box('adult').get(1) == null) &&
        (Hive.box('adult').get(3) == null) &&
        (Hive.box('adult').get(3) == null)) {
      setState(() {
        guest = "Select guest";
      });
    } else {
      setState(() {
        guest = "no value";
      });
    }
    super.initState();
  }
  @override

  Widget build(BuildContext context) {
    return Container(

        child: Padding(
          padding:  EdgeInsets.only(),
          child: GestureDetector(
            onTap: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>RoomSelectionPage()));
            },
            child: Container(
              height: screenHeight(context,dividedBy: 10),
              width:screenWidth(context,dividedBy:1.13),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      offset:Offset(-0.4, -0.4),
                      color:Color(0xFFDBE7EB),
                      blurRadius: 5
                  ),
                  BoxShadow(
                      offset:Offset(0.4, 0.4),
                      color:Color(0xFFDBE7EB),
                      blurRadius: 5
                  ),
                ],

                color: Colors.white,
                borderRadius: BorderRadius.all( Radius.circular(15)),
              ),

              child:Padding(
                padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 70)),
                child: RoomPersonTotalCount(person:"assets/icons/Person.svg",adultNum:guest == "no value"
                    ? "$adults" : "",
                  childNum:guest == "no value"
                      ? "$children" :"",roomNum: guest == "no value"
                      ? "$rooms" : "Select Guest", roomHeading: "Traveller",),
              ),


            ),
          ),
        )
    );
  }
}

