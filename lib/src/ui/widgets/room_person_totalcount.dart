import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class RoomPersonTotalCount extends StatefulWidget {
  @override
  String roomHeading;
  String adultNum;
  String roomNum;
  String childNum;
  String person;
  RoomPersonTotalCount({this.adultNum,this.childNum,this.roomNum,this.person,this.roomHeading});
  _RoomPersonTotalCountState createState() => _RoomPersonTotalCountState();
}

class _RoomPersonTotalCountState extends State<RoomPersonTotalCount> {
  @override
  Widget build(BuildContext context) {

    return Container(
        child:Column(
          children: [
            Stack(
              children: [
                Align(
                  child: Padding(
                    padding: EdgeInsets.only(right: screenWidth(context,dividedBy: 1.5),top: screenHeight(context,dividedBy:60 ) ),
                    child: SvgPicture.asset(widget.person,color: Color(0xFF464646),
                      height:screenHeight(context,dividedBy: 23),),
                  ),
                ),


                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:4.9),top:screenHeight(context,dividedBy: 500)),
                  child: Text(widget.roomHeading + " ",style:TextStyle(fontSize: 16,color:Color(0xFFC4C4C4),fontFamily:'Poppins',fontWeight: FontWeight.w500),),
                ),
                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:2.5),top:screenHeight(context,dividedBy:30 ),),
                  child: Text(widget.adultNum.toString() + widget.childNum.toString(),style:TextStyle(fontSize:18,color:Color(0xFF464646),fontFamily:'Noto sans',fontWeight: FontWeight.w400),),
                ),
                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:5),top:screenHeight(context,dividedBy:30)),
                  child: Text( widget.roomNum.toString(),style:TextStyle(fontSize: 18,color:Color(0xFF464646),fontFamily:'Noto sans',fontWeight: FontWeight.w800),),

                ),
              ],
            ),
          ],
        )
    );
  }
}
