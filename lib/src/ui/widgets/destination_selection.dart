import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hote_booking_4/Icons/city_icons.dart';
import 'package:hote_booking_4/src/ui/screens/destination_page.dart';
import 'package:hote_booking_4/src/utils/utils.dart';
class DestinationBox extends StatefulWidget {
  @override
  String DestinationName;
  DestinationBox({this.DestinationName});
  _DestinationBoxState createState() => _DestinationBoxState();
}

class _DestinationBoxState extends State<DestinationBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
            child:Container(
              height: screenHeight(context,dividedBy: 11),
              width:screenWidth(context,dividedBy:1.13),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      offset:Offset(-0.4, -0.4),
                      color:Color(0xFFDBE7EB),
                      blurRadius: 5
                  ),
                  BoxShadow(
                      offset:Offset(0.4, 0.4),
                      color:Color(0xFFDBE7EB),
                      blurRadius: 5
                  ),
                ],
               color: Colors.white,
                borderRadius: BorderRadius.all( Radius.circular(15),),
              ),
              child:
                  GestureDetector(
                    child: Padding(
                      padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:20 ),top: screenHeight(context,dividedBy:80)),
                      child: TextField(
                        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => DestinationPage()));},
                        style: TextStyle(color:Color(0xFF464646),fontFamily: 'Gilroy',fontWeight: FontWeight.w800,fontSize: 24),
                        cursorColor: Color(0xFF464646),
                        decoration: InputDecoration(
                          prefixIcon: Icon(City.search,color:Color(0xFF464646),size:30),
                          hintText: widget.DestinationName,
                          hintStyle: TextStyle(color:Color(0xFF464646),fontFamily: 'Gilroy',fontWeight: FontWeight.w800),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    onTap: () {},
                  ),



            ),
        ),
    );
  }
}
