import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/localization/locale.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/auth_service/firebase_auth.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/screens/login.dart';
import 'package:hote_booking_4/src/ui/screens/Login_page.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/screens/profile_page.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/screens/search_destination.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropcurrency.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropdown.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/constants.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class HomeDrawer extends StatefulWidget {
  final FirebaseAuth auth, authfb;

  const HomeDrawer({this.auth, this.authfb});

  @override
  // _HomeDrawerState createState() => _HomeDrawerState();
  State<StatefulWidget> createState() {
    return _HomeDrawerState();
  }
}

class _HomeDrawerState extends State<HomeDrawer> {
  String buttonName = "Sign_in";
  String photpurl;
  String dummyphotourl = 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'Username';

  @override
  void initState() {
    if ((widget.auth.currentUser != null) ||
        (widget.authfb.currentUser != null)) {
      setState(() {
        buttonName = "Sign_Out";
        photpurl = Hive.box('lang').get(12);
        displayName = Hive.box('lang').get(13);
      });
    } else {
      setState(() {
        photpurl = dummyphotourl;
        displayName = dummydisplayName;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          Padding(
            padding: EdgeInsets.only(left: 15.0),
            child: Row(
              children: [
                Container(
                  width: 38.0,
                  height: 40.24,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(.05),
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: NetworkImage(photpurl), fit: BoxFit.fill),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Text(displayName,
                      style: TextStyle(
                          color: Color(0xFF272727),
                          fontWeight: FontWeight.w300,
                          fontSize: 18.0,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'NexaLight')),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
            child: MaterialButton(
              color: Constants.kitGradients[0],
              onPressed: () {
                if ((widget.auth.currentUser == null) &&
                    (widget.authfb.currentUser == null)) {
                  // Navigator.pushAndRemoveUntil(
                  //  context,
                  //  MaterialPageRoute(
                  //      builder: (context) => Login(auth: widget.auth)),
                  //  (route) => false);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              Login(auth: widget.auth, authFb: widget.authfb)));
                } else {
                  void signout() async {
                    await Auth(auth: widget.auth, authfb: widget.authfb)
                        .Signout();
                  }

                  signout();

                  setState(() {
                    buttonName = "Sign in";
                    photpurl = dummyphotourl;
                    displayName = dummydisplayName;
                  });
                }
              },
              minWidth: 10,
              height: 40,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                buttonName,
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchDestination()));
              },
              child: ListTile(
                leading: SvgPicture.asset("assets/images/search_icon.svg"),
                title: Text(
                  getTranslated(context, 'Search'),
                  style: TextStyle(
                      color: Colors.black.withOpacity(.50),
                      fontWeight: FontWeight.w200),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfilePage(auth:widget.auth,authfb: widget.authfb,)));
              },
              child: ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.black.withOpacity(.60),
                ),
                title: Text(
                  getTranslated(context, 'Profile'),
                  style: TextStyle(
                      color: Colors.black.withOpacity(.60),
                      fontWeight: FontWeight.w200),
                ),
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 4.7),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 1),
                child: Text(
                  getTranslated(context, 'Region'),
                  style: TextStyle(fontSize: 12, color: Colors.black),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 0, 10),
                  child: DropCountry())
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 1),
                child: Text(
                  getTranslated(context, 'Currency'),
                  style: TextStyle(fontSize: 12, color: Colors.black),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 0, 10),
                child: DropCurrency(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
