import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropdown.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hote_booking_4/src/ui/widgets/build_dropcurrency.dart';
class RegionSelection extends StatefulWidget {
  @override

  _RegionSelectionState createState() => _RegionSelectionState();
}

class _RegionSelectionState extends State<RegionSelection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:
      Column(
        children:[
          Row(
        children: [
           Padding(
            padding:  EdgeInsets.only(top:screenHeight(context,dividedBy:8 ),left:screenWidth(context,dividedBy:40)),
            child: Container(
                height:screenHeight(context,dividedBy: 25),
                width:screenWidth(context,dividedBy: 4) ,

                decoration: BoxDecoration(


                ),
                child: Text("Region",style:TextStyle( fontSize:18,fontFamily:'Poppins',color: Color(0xFF404040),fontWeight: FontWeight.w400))),
          ),
           Padding(
            padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 4.4),top:screenHeight(context,dividedBy:8)),
            child: DropCountry(),
          )

        ],
      ),

       Row(
        children: [
          Padding(
            padding:  EdgeInsets.only(top:screenHeight(context,dividedBy:30 ),left:screenWidth(context,dividedBy:40)),
            child: Container(
                height:screenHeight(context,dividedBy: 25),
                width:screenWidth(context,dividedBy: 4) ,
                decoration: BoxDecoration(


                ),
                child: Text("Currency",style:TextStyle( fontSize:18,fontFamily:'Poppins',color: Color(0xFF404040),fontWeight: FontWeight.w400))),
          ),
          Padding(
            padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 3.7),top:screenHeight(context,dividedBy:25)),
            child: DropCurrency(),
          )

        ],
      ),
  ])

    );
  }
}
