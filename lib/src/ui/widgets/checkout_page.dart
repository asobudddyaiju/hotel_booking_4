import 'package:flutter/material.dart';
import 'package:hote_booking_4/src/ui/widgets/Checkout_date.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropdown.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/calender.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class CheckOut extends StatefulWidget {
  @override
  _CheckOutState createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOut> {
  @override
  Widget build(BuildContext context) {
    return Container(

        child: Stack(

            children: [

              Padding(
                padding:  EdgeInsets.only(),
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => DatePicker()));
                  },
                  child: Container(
                    height: screenHeight(context,dividedBy: 10),
                    width:screenWidth(context,dividedBy:1.13),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            offset:Offset(-0.4, -0.4),
                            color:Color(0xFFDBE7EB),
                            blurRadius: 5
                        ),
                        BoxShadow(
                            offset:Offset(0.4, 0.4),
                            color:Color(0xFFDBE7EB),
                            blurRadius: 5
                        ),
                      ],

                      color: Colors.white,
                      borderRadius: BorderRadius.all( Radius.circular(15)),
                    ),








                    child:Padding(
                      padding:  EdgeInsets.only(top:12),
                      child: Checkout_date(),
                     ),


                  ),
                ),
              ),







              Padding(
                padding:  EdgeInsets.only(left:screenWidth(context,dividedBy:2.05,),top: screenWidth(context,dividedBy:8 )),
                child: Container(
                  height: screenHeight(context,dividedBy: 30),
                  width:screenWidth(context,dividedBy: 2.5),
                  decoration: BoxDecoration(
                      color: Color(0xFF244F81),
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(25),bottomRight: Radius.circular(25),)
                  ),
                  child:Padding(
                    padding: EdgeInsets.only(left:screenHeight(context,dividedBy: 11),top: screenHeight(context,dividedBy:300)),
                    child: Text("Check-Out",style: TextStyle(fontSize:12,color: Colors.white,fontFamily:'Poppins',fontWeight: FontWeight.w600 ),),
                  ),
                ),
              ),






              Padding(
                padding: EdgeInsets.only(left: screenWidth(context,dividedBy:9.5,),top:screenHeight(context,dividedBy: 700)),
                child: Container(
                  height: screenHeight(context,dividedBy: 100),
                  width: screenWidth(context,dividedBy: 100),
                  decoration: BoxDecoration(
                    color:Color(0xFFC4C4C4),
                    shape: BoxShape.circle,



                  ),
                ),
              ),





              Padding(
                padding: EdgeInsets.only(left: screenWidth(context,dividedBy:9.5,),top:screenHeight(context,dividedBy: 40)),
                child: Container(
                  height: screenHeight(context,dividedBy: 100),
                  width: screenWidth(context,dividedBy: 100),
                  decoration: BoxDecoration(
                    color:Color(0xFFC4C4C4),
                    shape: BoxShape.circle,



                  ),
                ),),
              Padding(
                padding: EdgeInsets.only(left: screenWidth(context,dividedBy:9.5,),top:screenHeight(context,dividedBy: 70)),
                child: Container(
                  height: screenHeight(context,dividedBy: 100),
                  width: screenWidth(context,dividedBy: 100),
                  decoration: BoxDecoration(
                    color:Color(0xFFC4C4C4),
                    shape: BoxShape.circle,



                  ),
                ),
              )
            ] )
    );
  }
}

