import 'package:flutter/material.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropdown.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/calender.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/check_in_date.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class Checkin extends StatefulWidget {
  @override
  _CheckinState createState() => _CheckinState();
}

class _CheckinState extends State<Checkin> {
  @override
  Widget build(BuildContext context) {
    return Container(

        child: Stack(

            children: [

              Padding(
                padding:  EdgeInsets.only(),
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => DatePicker()));
                  },
                  child: Container(
                    height: screenHeight(context,dividedBy: 10),
                    width:screenWidth(context,dividedBy:1.13),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            offset:Offset(-0.4, -0.4),
                            color:Color(0xFFDBE7EB),
                            blurRadius: 5
                        ),
                        BoxShadow(
                            offset:Offset(0.4, 0.4),
                            color:Color(0xFFDBE7EB),
                            blurRadius: 5
                        ),
                      ],

                      color: Colors.white,
                      borderRadius: BorderRadius.all( Radius.circular(15)),
                    ),

                    child:Padding(
                      padding:  EdgeInsets.only(top:20),
                      child: Center(child:
                         Checkin_date()),
                    ),


                  ),
                ),
              ),
              Padding(
                padding:  EdgeInsets.only(left:screenWidth(context,dividedBy:2.05)),
                child: Container(
                  height: screenHeight(context,dividedBy: 30),
                  width:screenWidth(context,dividedBy: 2.5),
                  decoration: BoxDecoration(
                      color: Color(0xFF244F81),
                      borderRadius: BorderRadius.only(topRight: Radius.circular(25),bottomLeft: Radius.circular(25),)
                  ),
                  child:Padding(
                    padding: EdgeInsets.only(left:screenHeight(context,dividedBy: 9.5),top: screenHeight(context,dividedBy:300)),
                    child: Text("Check-in ",style: TextStyle(fontSize:12,color: Colors.white,fontFamily:'Poppins',fontWeight: FontWeight.w600 ),),
                  ),
                ),
              ),
            ] )
    );
  }
}

