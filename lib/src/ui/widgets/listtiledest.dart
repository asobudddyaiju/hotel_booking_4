import 'package:flutter/material.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class ListTiledest extends StatefulWidget {
  IconData Icon1;
  String  NamesCity;
  ListTiledest({ this.Icon1, this.NamesCity  });
  @override
  _ListTiledestState createState() => _ListTiledestState();
}

class _ListTiledestState extends State<ListTiledest> {
  @override
  Widget build(BuildContext context) {
    return
      ListTile(
        dense:true,
        contentPadding: EdgeInsets.only(left:screenWidth(context,dividedBy: 30),),
        leading:Icon(widget.Icon1,),

        title:Text(widget.NamesCity,style: TextStyle(color:Color(0xFF292929),fontFamily: 'Poppins',fontWeight: FontWeight.w300),
        ),

    );

  }
}
