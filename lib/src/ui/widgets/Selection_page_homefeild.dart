import 'package:flutter/material.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';
class SelectionPageHomefeild extends StatefulWidget {
  String name;
  String heading;
  double leftpad;
  Widget dropDown;
  SelectionPageHomefeild({this.name,this.heading,this.leftpad,this.dropDown});
  @override
  _SelectionPageHomefeildState createState() => _SelectionPageHomefeildState();
}

class _SelectionPageHomefeildState extends State<SelectionPageHomefeild> {
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

             Container(
              height: screenHeight(context,dividedBy: 30),
              width:screenWidth(context,dividedBy: 2.5),
              decoration: BoxDecoration(
                  color: Color(0xFF244F81),
                  borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10),)
              ),
              child:Padding(
                padding: EdgeInsets.only(left:screenHeight(context,dividedBy: 30),top: screenHeight(context,dividedBy:300)),
                child: Text(widget.heading,style: TextStyle(fontSize:12,color: Colors.white,fontFamily:'Poppins',fontWeight: FontWeight.w600 ),),
              ),
            ),



        Padding(
          padding:  EdgeInsets.only(),
            child: Container(
              height: screenHeight(context,dividedBy: 14),
              width:screenWidth(context,dividedBy:1.13),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      offset:Offset(-0.4, -0.4),
                      color:Color(0xFFDBE7EB),
                      blurRadius: 2
                  ),
                  BoxShadow(
                      offset:Offset(0.4, 0.4),
                      color:Color(0xFFDBE7EB),
                      blurRadius: 2
                  ),
                ],

                color: Colors.white,
                borderRadius: BorderRadius.only( topRight:Radius.circular(12),bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10),),
              ),
              child:Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 100),left:screenWidth(context,dividedBy:15 )),
                    child: widget.dropDown,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 100),left:screenWidth(context,dividedBy:widget.leftpad)),
                    child: Container
                      (
                        height: screenHeight(context,dividedBy: 10),
                        width:screenWidth(context,dividedBy: 5),

                        child: GestureDetector(
                          child: Padding(
                            padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 100)),
                            child: Text("change",style: TextStyle(fontSize:14,color: Colors.black,fontFamily:'Poppins',fontWeight: FontWeight.w600 ,),textAlign:TextAlign.left,),

                          ),
                          onTap: (){

                          },
                        )),
                  ),

                ],
              ),


            ),
        ),
      ] )
    );
  }
}
