import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/object_factory.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class Checkout_date extends StatefulWidget {
  @override

  _Checkout_dateState createState() => _Checkout_dateState();
}
String monthLetterCheckOut;
String CheckOutDate;
String CheckOutMonth;
String CheckOutYear;
String weekDayCheckOut;
String checkOutDay;

class _Checkout_dateState extends State<Checkout_date> {
  @override
  void initState() {
    setState(() {
      monthLetterCheckOut = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(5));
    });
    setState(() {
      weekDayCheckOut = ObjectFactory()
          .getDay
          .getWeekDay(Hive.box('code').get(25));
    });

    if (Hive.box('code').get(4) != null) {
      setState(() {
        CheckOutDate = Hive.box('code').get(4).toString();

      });
    } else {
      CheckOutDate= "Select Date";
    }
    if (Hive.box('code').get(25) != null) {
      setState(() {
        checkOutDay = weekDayCheckOut;

      });
    } else {
      checkOutDay= "";
    }

    if (Hive.box('code').get(6) != null) {
      setState(() {
        CheckOutMonth = monthLetterCheckOut;

      });
    } else {
      CheckOutMonth= "";
    }

    if (Hive.box('code').get(23) != null) {
      setState(() {
        CheckOutYear = Hive.box('code').get(23).toString();

      });
    } else {
      CheckOutYear= "";
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child:Column(
          children: [
            Stack(
              alignment: Alignment.topLeft,
              children: [

                Align(
                  child: Padding(
                    padding: EdgeInsets.only(right: screenWidth(context,dividedBy: 1.5),top:screenWidth(context,dividedBy:30) ),
                    child: SvgPicture.asset("assets/icons/Calendercheckout.svg",color: Color(0xFF464646),
                      height:screenHeight(context,dividedBy: 23),),
                  ),
                ),


                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:4.9),top:screenHeight(context,dividedBy: 27)),
                  child: Text(checkOutDay,style:TextStyle(fontSize: 10,color:Color(0xFF8F8F8F),fontFamily:'Noto Sans',fontWeight: FontWeight.w600),),
                ),
                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:3.8),top:screenHeight(context,dividedBy: 200),),
                  child: Text(CheckOutMonth + " " + CheckOutYear,style:TextStyle(fontSize:20,color:Color(0xFF464646),fontFamily:'Noto sans',fontWeight: FontWeight.w400),),
                ),
                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:5),top:screenHeight(context,dividedBy:200 )),
                  child: Text(CheckOutDate,style:TextStyle(fontSize: 20,color:Color(0xFF464646),fontFamily:'Noto sans',fontWeight: FontWeight.w800),),

                ),










              ],
            ),
          ],
        )
    );
  }
}
