import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/object_factory.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class Checkin_date extends StatefulWidget {
  @override
  _Checkin_dateState createState() => _Checkin_dateState();
}
String monthInLetterCheckIn;
String CheckInDate;
String CheckInMonth;
String CheckInYear;
String weekDayCheckIn;
String checkInDay;

class _Checkin_dateState extends State<Checkin_date> {
 @override
  void initState() {
   setState(() {
     monthInLetterCheckIn = ObjectFactory()
         .getMonthAlpha
         .getCheckInMonth(Hive.box('code').get(5));
   });
   setState(() {
     weekDayCheckIn = ObjectFactory()
         .getDay
         .getWeekDay(Hive.box('code').get(24));
   });

   if (Hive.box('code').get(5) != null) {
     setState(() {
       CheckInMonth = monthInLetterCheckIn;
     });
   } else {
     CheckInMonth= "";
   }
   if (Hive.box('code').get(24) != null) {
     setState(() {
       checkInDay = weekDayCheckIn;

     });
   } else {
     checkInDay= "";
   }

   if (Hive.box('code').get(3) != null) {
     setState(() {
       CheckInDate = Hive.box('code').get(3).toString();

     });
   } else {
     CheckInDate= "Select Date";
   }

   if (Hive.box('code').get(22) != null) {
     setState(() {
       CheckInYear = Hive.box('code').get(22).toString();

     });
   } else {
     CheckInYear= "";
   }
   super.initState();
 }
  @override
  Widget build(BuildContext context) {
    return Container(
       child:Column(
         children: [
           Stack(
             alignment: Alignment.topLeft,
             children: [

               Align(
                 child: Padding(
                   padding: EdgeInsets.only(right: screenWidth(context,dividedBy: 1.5) ),
                   child: SvgPicture.asset("assets/icons/Calendercheckin.svg",color: Color(0xFF464646),
                   height:screenHeight(context,dividedBy: 23),),
                 ),
               ),


               Padding(
                 padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:4.9),top:screenHeight(context,dividedBy: 27)),
                 child: Text(checkInDay,style:TextStyle(fontSize: 10,color:Color(0xFF8F8F8F),fontFamily:'Noto Sans',fontWeight: FontWeight.w600),),
               ),
               Padding(
                 padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:3.8),top:screenHeight(context,dividedBy: 200),),
                 child: Text(CheckInMonth + " " + CheckInYear ,style:TextStyle(fontSize:20,color:Color(0xFF464646),fontFamily:'Noto sans',fontWeight: FontWeight.w400),),
               ),
               Padding(
                 padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:5),top:screenHeight(context,dividedBy:200 )),
                 child: Text(CheckInDate,style:TextStyle(fontSize: 20,color:Color(0xFF464646),fontFamily:'Noto sans',fontWeight: FontWeight.w800),),

               ),

               Padding(
                 padding: EdgeInsets.only(left: screenWidth(context,dividedBy:10 ,),top:screenHeight(context,dividedBy: 23)),
                 child: Container(
                   height: screenHeight(context,dividedBy: 100),
                   width: screenWidth(context,dividedBy: 100),
                   decoration: BoxDecoration(
                     color:Color(0xFFC4C4C4),
                     shape: BoxShape.circle,



                   ),
                 ),
               ),
               Padding(
                 padding: EdgeInsets.only(left: screenWidth(context,dividedBy:10 ,),top:screenHeight(context,dividedBy: 19)),
                 child: Container(
                   height: screenHeight(context,dividedBy: 100),
                   width: screenWidth(context,dividedBy: 100),
                   decoration: BoxDecoration(
                     color:Color(0xFFC4C4C4),
                     shape: BoxShape.circle,



                   ),
                 ),),
               Padding(
                 padding: EdgeInsets.only(left: screenWidth(context,dividedBy:10 ,),top:screenHeight(context,dividedBy: 16)),
                 child: Container(
                   height: screenHeight(context,dividedBy: 100),
                   width: screenWidth(context,dividedBy: 100),
                   decoration: BoxDecoration(
                     color:Color(0xFFC4C4C4),
                     shape: BoxShape.circle,



                   ),
                 ),
               )








             ],
           ),
         ],
       )
    );
  }
}
