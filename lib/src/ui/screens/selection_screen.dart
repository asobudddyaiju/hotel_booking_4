
import 'package:flutter/material.dart';
import 'package:hote_booking_4/src/ui/screens/home_screen.dart';
import 'package:hote_booking_4/src/ui/widgets/Selection_page_homefeild.dart';
import 'package:hote_booking_4/src/ui/widgets/build_droplanguage.dart';
import 'package:hote_booking_4/src/ui/widgets/country_dropdown.dart';
import 'package:hote_booking_4/src/ui/widgets/currency_dropdown.dart';
import 'package:hote_booking_4/src/utils/utils.dart';


class SelectionPage extends StatefulWidget {
  @override
  _SelectionPageState createState() => _SelectionPageState();
}
TextEditingController _controller= TextEditingController();
class _SelectionPageState extends State<SelectionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
             resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body:Container(
        height: screenHeight(context,dividedBy: 1),
        width:  screenWidth(context,dividedBy: 1),

        child:Column(

          children: [
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left:screenWidth(context,dividedBy: 10),top:screenHeight(context,dividedBy:15)),
                  child: Text("Welcome",style:TextStyle(color:Color(0xFF3F4B6B),fontFamily: 'Gilroy',fontWeight: FontWeight.w800,fontSize:30 )),
                ),
                Padding(
                  padding: EdgeInsets.only(left:screenWidth(context,dividedBy: 50),top:screenHeight(context,dividedBy:15)),
                  child: Text("!",style:TextStyle(color:Color(0xFF3F4B6B),fontFamily: 'Gilroy-ExtraBold.otf',fontWeight: FontWeight.w800,fontSize:30 )),
                ),
              ],
            ),
            Padding(
              padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 7),left: screenWidth(context,dividedBy:30 )),
              child: SelectionPageHomefeild(heading:"Country",name: "India", leftpad:400, dropDown: CountryDropDown())
              ),


            Padding(
              padding:  EdgeInsets.only(top:screenWidth(context,dividedBy: 8),left: screenWidth(context,dividedBy:30 )),
              child: SelectionPageHomefeild(heading:"Currency",name:"Rupee",leftpad:400,dropDown: CurrencyDropdown()

              ),
            ),

            Padding(
              padding: EdgeInsets.only(top:screenWidth(context,dividedBy:8),left: screenWidth(context,dividedBy:30 )),
              child: SelectionPageHomefeild(heading:"Language",name:"Hindi",leftpad:400,dropDown: Language()),
            ),
        Padding(
         padding: EdgeInsets.only(top:screenHeight(context,dividedBy:12 ),left:screenWidth(context,dividedBy: 20),),
         child: Row(
          children: [
            Padding(
              padding:  EdgeInsets.only(),
              child: Text("Get started,",style: TextStyle(fontSize:18,fontFamily:'Poppins',fontWeight: FontWeight.w600 ,color:Color(0xFF244F81))),
            ),

            Padding(
              padding:  EdgeInsets.only(left:screenWidth(context,dividedBy: 2.5) ),
              child: Container(
                width: screenWidth(context,dividedBy: 5),
                height: screenHeight(context,dividedBy: 5),
                child: GestureDetector( onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) =>HomeScreen()));

                },
                    child: Icon(Icons.arrow_forward_ios, size: 30,color: Color(0xFFFFFFFF),)),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color(0xFF244F81)),
              )
            ),
          ],
        ),
      ),
  ] ),
            )
    );
  }
}
