import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hote_booking_4/src/ui/screens/Home_screen.dart';
import 'package:hote_booking_4/src/ui/screens/Login_page.dart';
import 'package:hote_booking_4/src/ui/screens/Selection_screen.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  // Timer _timerControl;
  //
  // void startTimer() {
  //    _timerControl = Timer .periodic(const Duration(seconds: 2), (timer) {
  //      Navigator.pushAndRemoveUntil(context,
  //         MaterialPageRoute(builder: (context) => SelectionPage()),
  //          (route) => false);
  //      _timerControl.cancel();
  //    });
  //  }

   @override
  //  void initState() {
  //    startTimer();
  //    super.initState();
  // }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
              resizeToAvoidBottomInset: false,
        body: Container(
          height: screenHeight(context,dividedBy: 1),
          width: screenWidth(context,dividedBy: 1),
          child:Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: screenWidth(context,dividedBy:1.3, ),top: screenHeight(context,dividedBy: 20)),
                  child: GestureDetector(
                    onTap: (){
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => SelectionPage()),
                              (route) => false);
                    },
                      child: Icon(Icons.arrow_back_ios,size:20 ,color: Color(0xFF7A7585),)

                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 11.4),),
                  child: SvgPicture.asset("assets/icons/Splashlogo.svg",
                  height:screenHeight(context,dividedBy: 8)),
                ),
                Padding(
                  padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 20)),
                  child: Text("hotels",style: TextStyle(color:Color(0xFF2F2F2F),fontSize:30,fontFamily: 'JosefinSans',fontWeight: FontWeight.w600 ),),
                ),
                Padding(
                  padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 34)),
                  child: Text("Version 125.2",style:TextStyle(color:Color(0xFF2F2F2F),fontSize:8,fontWeight: FontWeight.w300 ,fontFamily: 'Poppins')),
                ),
                Padding(
                  padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 700)),
                  child: Text("\u00a9 2020 hotello.com",style:TextStyle(color:Color(0xFF2F2F2F),fontSize:8,fontWeight: FontWeight.w300 ,fontFamily: 'Poppins')),
                ),
                Padding(
                  padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 19)),
                  child: Text("The best hotel deals",style: TextStyle(fontSize:16,color: Color(0xFF000000),fontWeight: FontWeight.w600 ,fontFamily: 'Poppins',),),
                ),
                Padding(
                  padding:  EdgeInsets.only(top:screenHeight(context,dividedBy:200 ),right: screenWidth(context,dividedBy: 8),left: screenWidth(context,dividedBy:8)),
                  child: Text("Compare the prices from all leading travel  agencies and hotel websites.booking.com, Expedia, Agoda and many more- in one app.",style: TextStyle(color:Color(0xFF000000),fontFamily: 'Poppins',fontSize: 12,fontWeight: FontWeight.w300),overflow: TextOverflow.clip,textAlign:TextAlign.justify,),
                ),
                Padding(
                  padding:EdgeInsets.only(top:screenHeight(context,dividedBy: 25)),
                  child: Image(image:AssetImage("assets/images/Splashbuild.png"))
                )
              ],
          )
        )

      ),
    );
    
  }
}
