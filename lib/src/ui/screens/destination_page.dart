import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hote_booking_4/Icons/city_icons.dart';
import 'package:hote_booking_4/src/ui/screens/Home_screen.dart';
import 'package:hote_booking_4/src/ui/widgets/Current_location.dart';
import 'package:hote_booking_4/src/ui/widgets/ListTiledest.dart';
import 'package:hote_booking_4/src/utils/utils.dart';


class DestinationPage extends StatefulWidget {
  @override

  _DestinationPageState createState() => _DestinationPageState();
}

class _DestinationPageState extends State<DestinationPage> {
  @override
  void initState() {

    super.initState();



    conditionCheck=false;

     }
  bool conditionCheck;
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          toolbarHeight: screenHeight(context,dividedBy:6),
        backgroundColor: Color(0xFFFFFFFF),
        actions: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding:  EdgeInsets.only(right: screenWidth(context,dividedBy: 1.13),top:screenWidth(context,dividedBy: 30)),
                child: GestureDetector(
                    child: Icon(Icons.arrow_back_ios,size:25,color: Color(0xFF7A7585),),
                  onTap: (){
                    Navigator.pushAndRemoveUntil(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()),
                            (route) => false);;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: screenWidth(context,dividedBy: 30),right: screenWidth(context,dividedBy:30 )),
                child: Container(
                  height: screenHeight(context,dividedBy: 15),
                  width: screenWidth(context,dividedBy: 1.14),


                  decoration: BoxDecoration(color: Color(0xFF244F81),
                    borderRadius:BorderRadius.circular(25),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(left:screenWidth(context,dividedBy:10),top:screenHeight(context,dividedBy:35),),
                    child: GestureDetector(
                      onTap: (){
                        // setState(()  {
                        //   widget.conditioncheck = true;
                        // });

                      },
                      child: TextField(
                        onTap: (){
                          setState(()  {
                            conditionCheck = true;
                          });
                        },
                         style: TextStyle(color: Colors.white,fontSize: 20,fontFamily: 'Poppins',fontWeight: FontWeight.w400),
                        cursorColor: Colors.white,
                        decoration: InputDecoration(
                          hintText: "Da Nang, Viet Nam",
                          hintStyle:TextStyle(color: Colors.white,fontSize: 20,fontFamily: 'Poppins',fontWeight: FontWeight.w400),
                          border: InputBorder.none,


                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],

      ),
          body: Column(
            children: [
               Row(children: [
                  Padding(
                 padding:  EdgeInsets.only(left:screenWidth(context,dividedBy: 30),top:screenHeight(context,dividedBy:30),),
                 child: SvgPicture.asset("assets/icons/Explorer.svg"),
                  ),

                 Padding(
                   padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 100),top:screenHeight(context,dividedBy: 35) ),
                   child: CurrentLocation(),
                 ),


                    ] ),
                conditionCheck == true ? Padding(
                padding: EdgeInsets.only(left:screenWidth(context,dividedBy: 20),top:screenHeight(context,dividedBy:40),),
                child: Container(
                  height: screenHeight(context,dividedBy: 1.6),
                  width: screenWidth(context,dividedBy: 1.1),
                  child: ListView.builder( itemCount:1,
                      itemBuilder:(Buildcontext, int index){
                             return Column(
                               children: [
                                 ListTiledest(Icon1:City.city, NamesCity: "London,England,United Kingdom",),

                                 ListTiledest(Icon1:City.plane1,NamesCity:"Lodon HealthRow Airport, London",),

                                 ListTiledest(Icon1:City.hotel,NamesCity:"Lodon HealthRow Airport, London",),

                                 ListTiledest(Icon1:City.island,NamesCity:"Lodon HealthRow Airport, London",),

                               ],
                             );


                      }) ),
              ):Container(),
            ],
          ),
          ) ;


  }
}
