import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hote_booking_4/src/ui/screens/destination_page.dart';
import 'package:hote_booking_4/src/ui/screens/drawer_login.dart';
import 'package:hote_booking_4/src/ui/widgets/Checkout_page.dart';
import 'package:hote_booking_4/src/ui/widgets/Destination_selection.dart';
import 'package:hote_booking_4/src/ui/widgets/check_in_page.dart';
import 'package:hote_booking_4/src/ui/widgets/room%20_selection.dart';
import 'package:hote_booking_4/src/utils/utils.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      drawer: DrawerLogin(),
      body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left:screenWidth(context,dividedBy: 19),top:screenHeight(context,dividedBy:13),),
                    child: GestureDetector(
                      onTap: () {
                         _globalKey.currentState.openDrawer();
                      },
                        child: Icon(Icons.short_text,color: Colors.black,size:50,)
                     ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left:screenWidth(context,dividedBy: 15),top:screenHeight(context,dividedBy:30),),
                    child: Row(
                      children: [
                        Text("Search",style: TextStyle(fontSize:28,color: Color(0xFF3F4B6B),fontWeight: FontWeight.w300,fontFamily: 'Gilroy'),),

                        Padding(
                          padding: EdgeInsets.only(left:screenWidth(context,dividedBy: 100)),
                          child: Text("hotels",style: TextStyle(fontSize:28,color: Color(0xFF3F4B6B),fontWeight: FontWeight.w900,fontFamily: 'Gilroy')),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 20,) ,top:screenWidth(context,dividedBy: 10)),
                    child: Container(
                      child: GestureDetector(
                          child: DestinationBox()),

                    ),
                  ),
                  Stack(
                    children:[
                   Padding(
                    padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 20),left: screenWidth(context,dividedBy: 19)),
                    child: Checkin(),
                  ),

                  Padding(
                    padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 5),left: screenWidth(context,dividedBy: 19)),
                    child:CheckOut(),
                  ),

                      Padding(
                        padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 8.3),left: screenWidth(context,dividedBy: 1.4),),
                        child: Container(
                            height: screenHeight(context,dividedBy: 10),
                            width:screenWidth(context,dividedBy:5.3),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color:Color(0xFF244F81,)),


                            child:Image.asset("assets/icons/Search.png",height:screenHeight(context,dividedBy:4),width: 100,),
                        ),
                      ),
                            ]),

                  Padding(
                    padding: EdgeInsets.only(top: screenHeight(context,dividedBy: 20),left:screenWidth(context,dividedBy: 19)),
                    child: RoomSelection(),
                  ),



                  Padding(
                    padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 19),left: screenWidth(context,dividedBy: 19),),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) =>DestinationPage()));
                      },
                      child: Container(
                          height: screenHeight(context,dividedBy: 10),
                          width:screenWidth(context,dividedBy:1.13),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color:Color(0xFF244F81,)),


                          child:Center(child: Text("Search for hotels",style:TextStyle(fontSize:22,fontFamily: 'Poppins',color:Colors.white)))
                      ),
                    ),
                  ),



                  




  ]),

    );
  }
}
