import 'package:flutter/material.dart';
import 'package:hote_booking_4/src/ui/screens/Login_page.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/screens/webview.dart';
import 'package:hote_booking_4/src/ui/widgets/Region_selection.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropcurrency.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropdown.dart';
import 'package:hote_booking_4/src/utils/utils.dart';
class DrawerLogin extends StatefulWidget {
  @override
  _DrawerLoginState createState() => _DrawerLoginState();
}

class _DrawerLoginState extends State<DrawerLogin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height:screenHeight(context,dividedBy:1),
      width: screenWidth(context,dividedBy: 1.34),
      child: new Drawer(


        child: Column(
          children: [

            Padding(
              padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 10),left: screenWidth(context,dividedBy: 500),),
              child: GestureDetector(
                onTap:(){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Login()));
                },
                child: Container(
                    height: screenHeight(context,dividedBy: 10),
                    width:screenWidth(context,dividedBy:1.5),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color:Color(0xFF003C83,)),


                    child:Center(child: Text("Sign in",style:TextStyle(fontSize:22,fontFamily: 'Poppins',color:Colors.white,fontWeight: FontWeight.w500)))
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 10),right: screenWidth(context,dividedBy: 11)),
              child: Container(
                  height: screenHeight(context,dividedBy:13),
                  width:screenWidth(context,dividedBy:1.13),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topRight:Radius.circular(30),bottomRight:Radius.circular(30)),
                      color:Color(0xFFFFFFFF)),


                  child:Padding(
                    padding:  EdgeInsets.only(right:screenWidth(context,dividedBy:11)),
                    child: Center(child: Text("About hotelsin app",style:TextStyle(fontSize:18,fontFamily: 'Poppins',color:Color(0xFF484848),fontWeight: FontWeight.w400))),
                  )
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 30),right: screenWidth(context,dividedBy: 11)),
              child: Container(
                  height: screenHeight(context,dividedBy:13),
                  width:screenWidth(context,dividedBy:1.13),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topRight:Radius.circular(30),bottomRight:Radius.circular(30)),
                      color:Color(0xFFFFFFFF)),


                  child:Padding(
                    padding:  EdgeInsets.only(right:screenWidth(context,dividedBy:11)),
                    child: Center(child: Text("Help and feedback",style:TextStyle(fontSize:18,fontFamily: 'Poppins',color:Color(0xFF484848),fontWeight: FontWeight.w400))),
                  )
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 30),right: screenWidth(context,dividedBy: 11)),
              child: Container(
                  height: screenHeight(context,dividedBy:13),
                  width:screenWidth(context,dividedBy:1.13),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topRight:Radius.circular(30),bottomRight:Radius.circular(30)),
                      color:Color(0xFFFFFFFF)),


                  child:Padding(
                    padding:  EdgeInsets.only(right:screenWidth(context,dividedBy:7)),
                    child: Center(child: Text("Privacy policy",style:TextStyle(fontSize:18,fontFamily: 'Poppins',color:Color(0xFF484848),fontWeight: FontWeight.w400))),
                  )
              ),
            ),

            Padding(
              padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 100)),
              child: RegionSelection(),
            ),

          ],
        ),
      ),
    );
  }
}
