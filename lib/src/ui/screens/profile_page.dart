import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/localization/locale.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/auth_service/firebase_auth.dart';
import 'package:hote_booking_4/src/ui/screens/Login_page.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/screens/home_page.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropcurrency.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/build_dropdown.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/ui/widgets/profile_list_tile.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/utils/utils.dart';

import 'login.dart';

class ProfilePage extends StatefulWidget {
  FirebaseAuth auth;
  FirebaseAuth authfb;
  ProfilePage({this.auth,this.authfb});
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String buttonName = "Sign_in";
  String photpurl;
  String dummyphotourl = 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'Username';
  @override
  void initState() {
    if ((widget.auth.currentUser != null) ||
        (widget.authfb.currentUser != null)) {
      setState(() {
        buttonName = "Sign_Out";
        photpurl = Hive.box('lang').get(12);
        displayName = Hive.box('lang').get(13);
      });
    } else {
      setState(() {
        photpurl = dummyphotourl;
        displayName = dummydisplayName;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 20)),
          // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomePage()));
              },
            ),
            title: Text(
              'Profile',
              style: TextStyle(
                  color: Colors.black54, fontWeight: FontWeight.normal),
            ),
            elevation: 0,
            backgroundColor: Colors.white,
          )),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Card(
            elevation: 0.5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 1),
                      child: Text(
                        getTranslated(context, 'Region'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(200, 12, 5, 10),
                        child: DropCountry())
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 1),
                      child: Text(
                        getTranslated(context, 'Currency'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(100, 12, 30, 10),
                      child: DropCurrency(),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: screenHeight(context, dividedBy: 15)),
          Card(
            elevation: 0.5,
            child: Column(children: [
              ProfileListTile(
                listText: 'About us',
                listText1: '',
              ),
              ProfileListTile(
                listText: 'Help and feedback',
                listText1: '',
              ),
              ProfileListTile(
                listText: 'Term and conditions',
                listText1: '',
              ),
              ProfileListTile(
                listText: 'Privacy policy',
                listText1: '',
              ),
              GestureDetector(
                onTap: () {
                  if ((widget.auth.currentUser == null) &&
                      (widget.authfb.currentUser == null)) {
                    // Navigator.pushAndRemoveUntil(
                    //  context,
                    //  MaterialPageRoute(
                    //      builder: (context) => Login(auth: widget.auth)),
                    //  (route) => false);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                Login(auth: widget.auth, authFb: widget.authfb)));
                  } else {
                    void signout() async {
                      await Auth(auth: widget.auth, authfb: widget.authfb)
                          .Signout();
                    }

                    signout();

                    setState(() {
                      buttonName = "Sign in";
                      photpurl = dummyphotourl;
                      displayName = dummydisplayName;
                    });
                  }
                },
                child: ProfileListTile(
                  listText: buttonName,
                  listText1: '',
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
