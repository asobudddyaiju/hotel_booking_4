
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hote_booking_4/src/ui/screens/home_screen.dart';
import 'package:hote_booking_4/src/ui/widgets/childrenCountUI.dart';
import 'package:hote_booking_4/src/ui/widgets/room_person_count.dart';
import 'package:hote_booking_4/src/utils/constants.dart';
import 'package:hote_booking_4/src/utils/utils.dart';

class RoomSelectionPage extends StatefulWidget {
  @override
  _RoomSelectionPageState createState() => _RoomSelectionPageState();
}

class _RoomSelectionPageState extends State<RoomSelectionPage> {
  @override
  var heightOfModalBottomSheet = 100.0;
  int counterValueRoom = 1;
  int counterValueAdults = 1;
  int counterValueChildren = 0;
  bool showLimit = false;
  List<String> agelist = [];
  String ageValue;
  String hint1, hint2, hint3;

  @override
  void initState() {
    if (Hive.box('adult').get(1) != null) {
      setState(() {
        counterValueRoom = int.parse(Hive.box('adult').get(1));
      });
    } else {
      setState(() {
        counterValueRoom = 1;
      });
    }
    if (Hive.box('adult').get(2) != null) {
      setState(() {
        counterValueAdults = int.parse(Hive.box('adult').get(2));
      });
    } else {
      setState(() {
        counterValueAdults = 1;
      });
    }
    if (Hive.box('adult').get(3) != null) {
      setState(() {
        counterValueChildren = int.parse(Hive.box('adult').get(3));
      });
    } else {
      setState(() {
        counterValueChildren = 0;
      });
    }
    if (Hive.box('adult').get(4) != null) {
      agelist = Hive.box('adult').get(4);
      if (agelist.length == 1) {
        setState(() {
          hint1 = agelist[0];
        });
      }
      if (agelist.length == 2) {
        setState(() {
          hint1 = agelist[0];
          hint2 = agelist[1];
        });
      }
      if (agelist.length == 3) {
        setState(() {
          hint1 = agelist[0];
          hint2 = agelist[1];
          hint3 = agelist[2];
        });
      }
    } else {
      setState(() {
        agelist = [];
      });
    }

    super.initState();
  }

  List<String> years = [
    "1 Years",
    "2 Years",
    "3 Years",
    "4 Years",
    "5 Years",
    "6 Years",
    "7 Years",
    "8 Years",
    "9 Years",
    "10 Years",
    "11 Years",
    "12 Years",
    "13 Years",
    "14 Years",
    "15 Years",
    "16 Years",
    "17 Years",
  ];
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        actions: [
          Padding(
            padding: EdgeInsets.only(right:screenHeight(context,dividedBy:30 )),
            child: GestureDetector(
              onTap: (){ Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()),
                      (route) => false);},
                child: Icon(Icons.close,size:30,color:Color(0xFF484848),)
            ),
          ),
          
          Padding(
            padding: EdgeInsets.only(right:screenWidth(context,dividedBy: 3.4,),top:screenHeight(context,dividedBy: 55)),
            child: Text("Rooms and Guests",style:TextStyle(color:Color(0xFF484848),fontSize: 24),),
          )
        ],

      ),
      body:Container(
        child:new Wrap(
          children : <Widget>[

            Padding(
              padding: EdgeInsets.only(top:screenHeight(context,dividedBy:50 ),),
              child: showLimit == true
                  ? Center(
                child: Container(
                  height: screenHeight(context, dividedBy: 30),
                  width: screenWidth(context, dividedBy: 1.18),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xFFFF645A)
                  ),
                  child: Center(
                     child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image(
                            image: AssetImage(Constants.CAUTION_SIGN),
                          ),
                          Text(
                            Constants.GUEST_LIMIT_ERROR,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 9.0,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Poppins',
                                fontStyle: FontStyle.normal),
                          ),
                        ],
                      ),

                  ),
                ),
              )
                  : Container(
                height: screenHeight(context, dividedBy: 20),
                width: screenWidth(context, dividedBy: 1.2),
              ),
            ),
            RoomPersonCount(
                counterLabel: "Room",
                counterValue: counterValueRoom,
                decFunction: () {
                  if (counterValueRoom > 1) {
                    setState(() {
                      counterValueRoom--;
                      Hive.box('adult').put(1,counterValueRoom.toString());
                      print("Rooms = "+Hive.box('adult').get(1).toString());

                    });
                  }
                  if (counterValueRoom <= 9) {
                    setState(() {

                      showLimit = false;
                    });
                  }
                },
                incFunction: () {
                  if (counterValueRoom < 9) {
                    setState(() {counterValueRoom++;
                    Hive.box('adult').put(1,counterValueRoom.toString());

                    });
                  } else {
                    setState(() {
                      showLimit = true;
                    });
                  }
                }),
            Center(
              child: Container(
                height: 1,
                width: screenWidth(context, dividedBy: 1.1),
                color: Constants.kitGradients[2].withOpacity(.06),
              ),
            ),

            RoomPersonCount(
              counterLabel: "Adult",
              counterValue: counterValueAdults,
              decFunction: () {
                if (counterValueAdults > 1) {
                  setState(() {
                    counterValueAdults--;
                    Hive.box('adult').put(2, counterValueAdults.toString());
                  });

                }
                if ((counterValueAdults + counterValueChildren) <= 16) {
                  setState(() {
                    showLimit = false;
                  });
                }
              },
              incFunction: () {
                if (counterValueAdults < counterValueRoom * 3 &&
                    counterValueRoom < 9) {
                  setState(() {
                    counterValueAdults++;
                    Hive.box('adult').put(2, counterValueAdults.toString());
                  });
                } else if (counterValueRoom < 9) {
                  setState(() => counterValueRoom++);
                  setState(() => counterValueAdults++);
                  setState(()  =>  Hive.box('adult').put(2, counterValueAdults.toString()));
                }
                if ((counterValueAdults + counterValueChildren) > 16) {
                  setState(() {
                    showLimit = true;
                  });
                }
              },
            ),
            Center(
              child: Container(
                height: 1,
                width: screenWidth(context, dividedBy: 1.1),
                color: Constants.kitGradients[2].withOpacity(.06),
              ),
            ),

            RoomPersonCount(
              counterLabel: "Children",
              counterValue: counterValueChildren,
              incFunction: () {
                if (counterValueAdults > 0 &&
                    ((counterValueAdults + counterValueChildren) <
                        counterValueRoom * 3) &&
                    counterValueChildren <3 ) {
                  setState(() => counterValueChildren++);
                  setState(()=>Hive.box('adult').put(3, counterValueChildren.toString()));

                }
                if ((counterValueAdults + counterValueChildren) > 16) {
                  setState(() {
                    showLimit = true;
                  });
                }
              },
              decFunction: () {
                if (counterValueChildren > 0) {
                  setState(() => counterValueChildren--);
                  setState(() => Hive.box('adult').put(3, counterValueChildren.toString()));
                  if (counterValueChildren == 0) {
                    setState(() {
                      agelist.removeAt(0);
                    });
                  }
                  if (counterValueChildren == 1) {
                    setState(() {
                      agelist.removeAt(1);
                      agelist.removeAt(2);
                    });
                  }
                }
                if (counterValueChildren == 2) {
                  setState(() {
                    agelist.removeAt(2);
                  });
                }

                if ((counterValueAdults + counterValueChildren) <= 16) {
                  setState(() {
                    showLimit = false;
                  });
                }
              },
            ),
            counterValueChildren > 0
                ? Container(
              height: screenHeight(context, dividedBy: 30),
              width: screenWidth(context, dividedBy: 1.2),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 19.5),
                  ),
                  Text(
                    Constants.AGE_OF_CHILD,
                    style: TextStyle(
                        color: Constants.kitGradients[2],
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Poppins',
                        fontStyle: FontStyle.normal),
                  ),
                ],
              ),
            )
                : Container(
              height: screenHeight(context, dividedBy: 40),
              width: screenWidth(context, dividedBy: 1.2),
            ),
            counterValueChildren > 0
                ? ChildrenCountUI(
              hintText: hint1,
              childrenCount: 1,
              getage: (val) {
                ageValue = val;
                if (agelist.isNotEmpty) {
                  setState(() {
                    agelist[0] = ageValue;
                  });

                  Hive.box('adult').put(4, agelist);
                } else {
                  agelist.add(ageValue);
                  Hive.box('adult').put(4, agelist);
                }
              },
            )
                : Container(
              height: 0,
              width: 0,
            ),

            counterValueChildren > 1
                ? ChildrenCountUI(
              hintText: hint2,
              childrenCount: 2,
              getage: (val2) {
                ageValue = val2;
                if (agelist.length == 1) {
                  agelist.add(ageValue);
                  Hive.box('adult').put(4, agelist);
                } else {
                  setState(() {
                    agelist[1] = ageValue;
                  });
                  Hive.box('adult').put(4, agelist);
                }
              },
            )
                : Container(
              height: 0,
              width: 0,
            ),
            counterValueChildren > 2
                ? ChildrenCountUI(
              hintText: hint3,
              childrenCount: 3,
              getage: (val2) {
                ageValue = val2;
                if (agelist.length == 2) {
                  agelist.add(ageValue);
                  Hive.box('adult').put(4, agelist);
                } else {
                  setState(() {
                    agelist[2] = ageValue;
                  });
                  Hive.box('adult').put(4, agelist);
                }
              },
            )
                : Container(
              height: 0,
              width: 0,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),

            SizedBox(
              height: 70,
            ),
            //  ListTile(
            //   title: new Container(height: 1,width: screenWidth(context,dividedBy: 1.5),color: Constants.kitGradients[2].withOpacity(.06),),
            //   onTap: () => {},
            // ),
          ],
        ),
      ),
    );
  }
}
