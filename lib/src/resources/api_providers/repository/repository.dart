import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/models/state.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> sampleCall() => userApiProvider.sampleCall();

  Future<State> destinationList({String searchKey}) =>
      userApiProvider.destinationList(searchKey: searchKey);
}
