import 'package:flutter/cupertino.dart';
import 'file:///C:/Users/dison/AndroidStudioProjects/hotel_booking_4/lib/localization/demo_local.dart';

String getTranslated(BuildContext context, String key) {
  return DemoLocalizations.of(context).getTranslatedValues(key);
}
